﻿using System;
using System.Linq;
using System.Threading;
using Contracts;
using Ninject;
using Ninject.Extensions.NamedScope;
using Ninject.Parameters;
using NLog;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Persistence.Legacy;
using Warehouse.Domain;
using Warehouse.Domain.Entities;
using Warehouse.Messaging;

namespace Warehouse.Service
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            IKernel kernel = GetNinjectKernel();

            BusConfiguration busConfiguration = GetBusConfiguration(kernel);

            using (IBus bus = Bus.Create(busConfiguration).Start())
            {
                Logger.Trace("Warehouse service bus is up.");

                PublishAvailableProducts(bus, kernel);

                while (true)
                {
                    try
                    {
                        Console.WriteLine("Add new rent item: ");
                        string rentItemName = Console.ReadLine();
                        using (IProductContext context = kernel.Get<DbContextFactory>().CreateProductContext())
                        {
                            int randomProductTypeInt = new Random(DateTimeOffset.UtcNow.Millisecond).Next(1, 3);
                            var product = Product.Create(rentItemName, (ProductType)randomProductTypeInt);
                            context.AddProduct(product);
                            context.SaveChanges();

                            new ProductEventPublisher(bus).ProductWasAdded(product);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                    }
                }
            }
        }

        private static void PublishAvailableProducts(IBus bus, IKernel kernel)
        {
            using (IProductContext context = kernel.Get<DbContextFactory>().CreateProductContext())
            {
                foreach (Product product in context.AllProducts().Where(product => product.IsAvailableForRent()))
                {
                    // MSMQ migh time out otherwise
                    Thread.Sleep(10);

                    new ProductEventPublisher(bus).ProductWasAdded(product);
                }
            }
        }

        private static IKernel GetNinjectKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<DbContextFactory>()
                .ToSelf()
                .InSingletonScope()
                .WithConstructorArgument("DomainContextConnection");

            return kernel;
        }

        private static BusConfiguration GetBusConfiguration(IKernel ninjectKernel)
        {
            BusConfiguration busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName(GlobalSettings.Default.WarehouseBusEndpointName);
            busConfiguration.UseSerialization<JsonSerializer>();
            busConfiguration.EnableInstallers();
            busConfiguration.DisableFeature<TimeoutManager>();
            busConfiguration.UsePersistence<MsmqPersistence>();

            busConfiguration.UseContainer<NinjectBuilder>(containerCustomizations => containerCustomizations.ExistingKernel(ninjectKernel));

            busConfiguration.Conventions().DefiningEventsAs(type => type.Namespace != null
                && type.Namespace.StartsWith("Contracts")
                && type.Namespace.EndsWith("Events"));

            busConfiguration.Conventions().DefiningCommandsAs(type => type.Namespace != null
                && type.Namespace.StartsWith("Contracts")
                && type.Namespace.EndsWith("Commands"));

            busConfiguration.AssembliesToScan(typeof(ProductDto).Assembly, typeof(OrderEventHandler).Assembly);

            return busConfiguration;
        }
    }
}
