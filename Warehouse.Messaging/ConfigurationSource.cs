﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using Contracts.Commands;
using NServiceBus.Config;
using NServiceBus.Config.ConfigurationSource;

namespace Warehouse.Messaging
{
    public class ConfigurationSource : IProvideConfiguration<UnicastBusConfig>
    {
        public UnicastBusConfig GetConfiguration()
        {
            return new UnicastBusConfig
            {
                MessageEndpointMappings = new MessageEndpointMappingCollection
                {
                    new MessageEndpointMapping
                    {
                        AssemblyName= typeof(ProductDto).Assembly.GetName().Name,
                        TypeFullName = typeof(PlaceOrderCommand).FullName,
                        Endpoint = GlobalSettings.Default.RetailerBusEndpointName
                    }
                }
            };
        }
    }
}
