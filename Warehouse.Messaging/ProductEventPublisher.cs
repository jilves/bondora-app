﻿using System;
using Contracts;
using Contracts.Events;
using NLog;
using NServiceBus;
using Warehouse.Domain.Entities;

namespace Warehouse.Messaging
{
    public class ProductEventPublisher
    {
        private readonly IBus _bus;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ProductEventPublisher(IBus bus)
        {
            _bus = bus;
        }

        public void ProductWasAdded(Product product)
        {
            try
            {

                Logger.Trace($"Publish ProductWasAddedEvent for productId: {product.Id}, Name: {product.Name}");

                _bus.Publish(MapEntityToProductEvent<ProductWasAddedEvent>(product));
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public virtual void ProductWasUpdated(Product product)
        {
            try
            {
                Logger.Trace($"Publish ProductWasUpdated for productId: {product.Id}, IsAvailableForRent: {product.IsAvailableForRent()}");

                _bus.Publish(MapEntityToProductEvent<ProductWasUpdatedEvent>(product));
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        private T MapEntityToProductEvent<T>(Product product) where T : ProductEventBase, new()
        {
            return new T
            {
                Product = new ProductDto
                {
                    Id = product.Id,
                    Name = product.Name,
                    ProductType = product.ProductType,
                    IsAvailableForRent = product.IsAvailableForRent()
                }
            };
        }
    }
}
