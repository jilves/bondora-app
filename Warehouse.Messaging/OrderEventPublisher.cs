﻿using System;
using Contracts;
using Contracts.Events;
using NLog;
using NServiceBus;

namespace Warehouse.Messaging
{
    public class OrderEventPublisher
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IBus _bus;

        public OrderEventPublisher(IBus bus)
        {
            _bus = bus;
        }

        public virtual void OrderWasProcessed(Guid orderId, OrderStatusCode statusCode)
        {
            try
            {
                Logger.Trace($"Publish OrderWasProcessed for OrderId: {orderId}, StatusCode: {statusCode}");

                _bus.Publish(new OrderWasProcessedEvent
                {
                    OrderId = orderId,
                    StatusCode = statusCode
                });
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
    }
}
