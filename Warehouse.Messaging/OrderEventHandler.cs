﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using Contracts.Commands;
using NLog;
using NServiceBus;
using Warehouse.Domain;
using Warehouse.Domain.Entities;

namespace Warehouse.Messaging
{
    public class OrderEventHandler : IHandleMessages<PlaceOrderCommand>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly object LockObject = new object();

        private readonly OrderEventPublisher _orderEventPublisher;
        private readonly ProductEventPublisher _productEventPublisher;
        private readonly DbContextFactory _dbContextFactory;

        public OrderEventHandler(OrderEventPublisher orderEventPublisher,
            ProductEventPublisher productEventPublisher,
            DbContextFactory dbContextFactory)
        {
            _orderEventPublisher = orderEventPublisher;
            _productEventPublisher = productEventPublisher;
            _dbContextFactory = dbContextFactory;
        }


        // TODO: Test
        // Test cases
        // 1. Duplicate orders
        // 2. No orders (empty or null orderitems)
        // 3. Concurrent requests for same products from different orders
        // 4. Same order sent multiple times
        // 5. Duplicate orderitems
        // ??

        public void Handle(PlaceOrderCommand message)
        {
            // TODO: Is it correct to use a lock here?
            lock (LockObject)
            {
                Logger.Trace($"Received PlaceOrderCommand for OrderId : {message.OrderId}, Items: {string.Join(",", message.OrderedProducts.Select(x => x.ProductId))}");

                if (message.OrderId == Guid.Empty)
                {
                    return;
                }

                try
                {
                    HandleMessage(message);
                }
                catch (Exception e)
                {
                    Logger.Error(e);

                    _orderEventPublisher.OrderWasProcessed(message.OrderId, OrderStatusCode.InternalError);
                    throw;
                }
            }
        }

        private void HandleMessage(PlaceOrderCommand message)
        {
            Logger.Trace("HandleMessage PlaceOrderCommand");

            var orderedProductIds = message
                .OrderedProducts
                .Select(orderedProduct => orderedProduct.ProductId)
                .ToList();

            using (IProductContext productContext = _dbContextFactory.CreateProductContext())
            {
                var allAvailableProducts = productContext
                    .AllProducts()
                    .Where(product => product.IsAvailableForRent());

                var validOrderedProductsCount = allAvailableProducts
                    .Count(product => orderedProductIds.Contains(product.Id));

                var isOrderStockQuantitySatisfied = validOrderedProductsCount == orderedProductIds.Count;
                if (!isOrderStockQuantitySatisfied)
                {
                    _orderEventPublisher.OrderWasProcessed(message.OrderId, OrderStatusCode.OutOfStock);
                    return;
                }
            }

            using (IOrderContext orderContext = _dbContextFactory.CreateOrderContext())
            {
                var orderItems = orderedProductIds
                    .Select(OrderItem.Create)
                    .ToList();

                var order = Domain.Entities.Order.Create(message.OrderId, orderItems);

                orderContext.AddOrder(order);
                orderContext.SaveChanges();

                _orderEventPublisher.OrderWasProcessed(message.OrderId, OrderStatusCode.Success);
            }

            using (IProductContext productContext = _dbContextFactory.CreateProductContext())
            {
                var orderedProducts = productContext
                    .AllProducts()
                    .Where(product => orderedProductIds.Contains(product.Id))
                    .ToList();

                orderedProducts.ForEach(_productEventPublisher.ProductWasUpdated);
            }
        }
    }
}
