﻿using Contracts;
using NServiceBus.Config;
using NServiceBus.Config.ConfigurationSource;

namespace Retailer.Messaging
{
    public class ConfigurationSource : IProvideConfiguration<UnicastBusConfig>
    {
        public UnicastBusConfig GetConfiguration()
        {
            return new UnicastBusConfig
            {
                MessageEndpointMappings = new MessageEndpointMappingCollection
                {
                    new MessageEndpointMapping
                    {
                        AssemblyName= typeof(ProductDto).Assembly.GetName().Name,
                        Endpoint = GlobalSettings.Default.WarehouseBusEndpointName
                    }
                }
            };
        }
    }
}
