﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using Contracts.Commands;
using NLog;
using NServiceBus;
using Retailer.Domain.Entities;

namespace Retailer.Messaging
{
    public class OrderCommandSender
    {
        private readonly IBus _bus;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public OrderCommandSender(IBus bus)
        {
            _bus = bus;
        }

        public void PlaceOrder(Cart cart)
        {
            try
            {
                Logger.Trace($"Publish PlaceOrder comand for cartId: {cart.Id}");

                _bus.Send(GlobalSettings.Default.WarehouseBusEndpointName, new PlaceOrderCommand
                {
                    OrderId = cart.Id,
                    OrderedProducts = cart.CartItems.Select(cartItem => new OrderedProductDto
                    {
                        ProductId = cartItem.ProductId,
                        RentPeriodInDays = cartItem.RentPeriodInDays
                    }).ToList()
                });
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
    }
}
