﻿using System;
using System.Threading.Tasks;
using Contracts.Events;
using NLog;
using NServiceBus;
using Retailer.Domain;
using Retailer.Domain.Entities;

namespace Retailer.Messaging
{
    public class ProductEventHandler : IHandleMessages<ProductWasAddedEvent>,
        IHandleMessages<ProductWasUpdatedEvent>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IProductContext _productContext;

        public ProductEventHandler(IProductContext productContext)
        {
            _productContext = productContext;
        }

        public void Handle(ProductWasAddedEvent message)
        {
            try
            {
                Logger.Trace($"Received ProductWasAddedEvent. Dto: {message.Product}");

                var newProduct = Product.Create(
                    message.Product.Id,
                    message.Product.Name,
                    message.Product.IsAvailableForRent,
                    message.Product.ProductType);

                _productContext.AddProduct(newProduct);
                _productContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public void Handle(ProductWasUpdatedEvent message)
        {
            try
            {
                Logger.Trace($"Received ProductWasUpdatedEvent. Dto: {message.Product}");

                var productTask = Task.Run(async () =>
                {
                    var product = await _productContext.FindProductByIdOrDefaultAsync(message.Product.Id);

                    product.UpdateProduct(message.Product);

                    _productContext.SaveChanges();
                });

                productTask.Wait();

                if (productTask.Exception != null)
                {
                    throw productTask.Exception;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
    }
}
