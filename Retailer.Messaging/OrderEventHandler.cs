﻿using System;
using System.Threading.Tasks;
using Contracts.Events;
using NLog;
using NServiceBus;
using Retailer.Domain;

namespace Retailer.Messaging
{
    public class OrderEventHandler : IHandleMessages<OrderWasProcessedEvent>
    {
        private readonly ICartContext _cartContext;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public OrderEventHandler(ICartContext cartContext)
        {
            _cartContext = cartContext;
        }

        public void Handle(OrderWasProcessedEvent message)
        {
            try
            {
                Logger.Trace($"Received OrderWasProcessedEvent for CartId: {message.OrderId} with StatusCode: {message.StatusCode}");

                var cartTask = Task.Run(async () =>
                {
                    var cart = await _cartContext.GetOrCreateCartAsync(message.OrderId);

                    cart.UpdateStatus(message.StatusCode);

                    await _cartContext.SaveChangesAsync();
                });

                cartTask.Wait();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
    }
}
