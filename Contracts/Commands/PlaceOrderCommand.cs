﻿using System;
using System.Collections.Generic;

namespace Contracts.Commands
{
    public class PlaceOrderCommand
    {
        public Guid OrderId { get; set; }

        public IEnumerable<OrderedProductDto> OrderedProducts { get; set; }
    }
}
