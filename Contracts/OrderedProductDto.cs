﻿using System;

namespace Contracts
{
    public class OrderedProductDto
    {
        public Guid ProductId { get; set; }
        public int RentPeriodInDays { get; set; }
    }
}
