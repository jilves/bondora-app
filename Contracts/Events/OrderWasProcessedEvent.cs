﻿using System;

namespace Contracts.Events
{
    public class OrderWasProcessedEvent
    {
        public Guid OrderId { get; set; }
        public OrderStatusCode StatusCode { get; set; }
    }
}
