﻿namespace Contracts.Events
{
    public class ProductEventBase
    {
        public ProductDto Product { get; set; }
    }
}
