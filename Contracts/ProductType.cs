﻿namespace Contracts
{
    public enum ProductType
    {
        HeavyEquipment = 1,
        RegularEquipment = 2,
        SpecializedEquipment = 3
    }
}
