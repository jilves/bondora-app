﻿namespace Contracts
{
    public enum OrderStatusCode
    {
        Success = 1,
        OutOfStock = 2,
        InternalError = 3
    }
}
