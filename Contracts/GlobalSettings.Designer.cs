﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Contracts {
    
    
    [CompilerGenerated()]
    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    public sealed partial class GlobalSettings : ApplicationSettingsBase {
        
        private static GlobalSettings defaultInstance = ((GlobalSettings)(Synchronized(new GlobalSettings())));
        
        public static GlobalSettings Default {
            get {
                return defaultInstance;
            }
        }
        
        [ApplicationScopedSetting()]
        [DebuggerNonUserCode()]
        [DefaultSettingValue("Warehouse.Service#LOCAL")]
        public string WarehouseBusEndpointName {
            get {
                return ((string)(this["WarehouseBusEndpointName"]));
            }
        }
        
        [ApplicationScopedSetting()]
        [DebuggerNonUserCode()]
        [DefaultSettingValue("Retailer.WebUi#LOCAL")]
        public string RetailerBusEndpointName {
            get {
                return ((string)(this["RetailerBusEndpointName"]));
            }
        }
    }
}
