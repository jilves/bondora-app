﻿using System;

namespace Contracts
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailableForRent { get; set; }
        public ProductType ProductType { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, IsAvailableForRent: {IsAvailableForRent}, ProductType: {ProductType}";
        }
    }
}
