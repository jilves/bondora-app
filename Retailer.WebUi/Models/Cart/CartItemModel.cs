﻿namespace Retailer.WebUi.Models.Cart
{
    public class CartItemModel
    {
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string RentPeriodInDays { get; set; }
    }
}
