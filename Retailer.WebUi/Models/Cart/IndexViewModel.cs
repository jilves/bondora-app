﻿using System.Collections.Generic;

namespace Retailer.WebUi.Models.Cart
{
    public class IndexViewModel
    {
        public List<CartItemModel>  CartItems { get; set; }
        public bool IsCartConfirmed { get; set; }
        public string CartStatusInfo { get; set; }
        public string CartId { get; set; }
    }
}
