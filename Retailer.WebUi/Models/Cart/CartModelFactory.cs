﻿using System;
using System.Globalization;
using System.Linq;
using Contracts;

namespace Retailer.WebUi.Models.Cart
{
    public class CartModelFactory
    {
        public IndexViewModel CreateIndexViewModel(Domain.Entities.Cart cart)
        {
            return new IndexViewModel
            {
                CartId = cart.Id.ToString(),
                IsCartConfirmed = cart.IsConfirmed,
                CartStatusInfo = GetCartStatusInfo(cart),
                CartItems = cart.CartItems.Select(cartItem => new CartItemModel
                {
                    ProductName = cartItem.ProductName,
                    ProductType = cartItem.ProductType.GetProductTypeDisplayName(),
                    RentPeriodInDays = cartItem.RentPeriodInDays.ToString(),
                })
                .OrderBy(cartItem => cartItem.ProductType)
                .ThenBy(cartItem => cartItem.ProductName)
                .ToList()
            };
        }

        private string GetCartStatusInfo(Domain.Entities.Cart cart)
        {
            if (!cart.IsConfirmed)
            {
                return "Not confirmed";
            }

            switch (cart.OrderStatusCode)
            {
                case OrderStatusCode.Success:
                    return "Warehouse: Success.";
                case OrderStatusCode.OutOfStock:
                    return "Warehouse: One or more items you have ordered are out of stock. Please create a new order.";
                case OrderStatusCode.InternalError:
                    return "Warehouse: Internal error.";
                case null:
                    return "Cart is confirmed. Waiting for warehouse response.";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
