﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Retailer.WebUi.Models
{
    public class InvoiceGenerator
    {
        public async Task<byte[]> GeneratePlainTextInvoiceAsync(Domain.Entities.Cart cart)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream, Encoding.Unicode))
                {
                    await streamWriter.WriteLineAsync("Invoice title");

                    await streamWriter.WriteLineAsync();
                    await streamWriter.WriteLineAsync();

                    foreach (var cartItem in cart.CartItems)
                    {
                        await streamWriter.WriteLineAsync(
                            $"Product: {cartItem.ProductName}, Rental price: {cartItem.CalculateCost()} EUR, Loyality points: {cartItem.CalculateLoyalityPoints()}");
                    }

                    await streamWriter.WriteLineAsync();
                    await streamWriter.WriteLineAsync();

                    await streamWriter.WriteLineAsync($"TotalCost: {cart.CalculateTotalCost()} EUR");
                    await streamWriter.WriteLineAsync($"Loyality points earned: {cart.CalculateTotalLoyalityPoints()}");

                    await streamWriter.WriteLineAsync();
                }

                return memoryStream.ToArray();
            }
        }
    }
}
