﻿using System;
using Contracts;

namespace Retailer.WebUi.Models
{
    public static class ProductTypeExtensions
    {
        public static string GetProductTypeDisplayName(this ProductType productType)
        {
            switch (productType)
            {
                case ProductType.HeavyEquipment:
                    return "Heavy equipment";
                case ProductType.RegularEquipment:
                    return "Regular equipment";
                case ProductType.SpecializedEquipment:
                    return "Specialized equipment";
                default:
                    throw new ArgumentOutOfRangeException(nameof(productType), productType, null);
            }
        }
    }
}
