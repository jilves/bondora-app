﻿using System.Collections.Generic;

namespace Retailer.WebUi.Models.Home
{
    public class IndexViewModel
    {
        public List<ProductModel> Products { get; set; }
    }
}
