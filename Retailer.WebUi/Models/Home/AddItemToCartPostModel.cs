﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Retailer.WebUi.Models.Home
{
    public class AddItemToCartPostModel
    {
        public Guid ProductId { get; set; }

        [Required]
        [Range(1, 100)]
        public int RentPeriodInDays { get; set; }

        public string ReturnUrl { get; set; }
    }
}
