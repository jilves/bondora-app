﻿using System;

namespace Retailer.WebUi.Models.Home
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProductType { get; set; }
    }
}
