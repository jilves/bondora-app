﻿using System.Collections.Generic;
using System.Linq;
using Retailer.Domain.Entities;

namespace Retailer.WebUi.Models.Home
{
    public class HomeModelFactory
    {
        public IndexViewModel CreateIndexViewModel(Domain.Entities.Cart currentUsersCart, IEnumerable<Product> products)
        {
            var currentUsersProductIdsInCart = currentUsersCart
                .CartItems
                .Select(cartItem => cartItem.ProductId)
                .ToList();

            return new IndexViewModel
            {
                Products = products
                // Don't show items already in current users cart
                .Where(product => !currentUsersProductIdsInCart.Contains(product.Id))
                .Select(product => new ProductModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    ProductType = product.ProductType.GetProductTypeDisplayName()
                })
                .OrderBy(productModel => productModel.ProductType)
                .ThenBy(productModel => productModel.Name)
                .ToList()
            };
        }
    }
}
