﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Retailer.Domain;
using Retailer.WebUi.Models.Home;

namespace Retailer.WebUi.Controllers
{
    public class HomeController : BaseController
    {
        private readonly HomeModelFactory _homeModelFactory;
        private readonly IProductContext _productContext;
        private readonly ICartContext _cartContext;

        public HomeController(HomeModelFactory homeModelFactory, IProductContext productContext, ICartContext cartContext)
        {
            _homeModelFactory = homeModelFactory;
            _productContext = productContext;
            _cartContext = cartContext;
        }

        public async Task<ActionResult> Index()
        {
            var cart = await _cartContext.GetOrCreateCartAsync(SessionsCartId);
            var products = await _productContext.GetProductsAvailableForRentAsync();
            var viewModel = _homeModelFactory.CreateIndexViewModel(cart, products);

            return View(viewModel);
        }
    }
}