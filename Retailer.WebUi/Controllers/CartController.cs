﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Retailer.Domain;
using Retailer.Messaging;
using Retailer.WebUi.Models.Cart;
using Retailer.WebUi.Models.Home;

namespace Retailer.WebUi.Controllers
{
    public class CartController : BaseController
    {
        private readonly CartModelFactory _cartModelFactory;
        private readonly OrderCommandSender _orderEventPublisher;
        private readonly IProductContext _productContext;
        private readonly ICartContext _cartContext;

        public CartController(CartModelFactory cartModelFactory,
            OrderCommandSender orderEventPublisher,
            IProductContext productContext, 
            ICartContext cartContext)
        {
            _cartModelFactory = cartModelFactory;
            _orderEventPublisher = orderEventPublisher;
            _productContext = productContext;
            _cartContext = cartContext;
        }

        public async Task<ActionResult> Index()
        {
            var cart = await _cartContext.GetOrCreateCartAsync(SessionsCartId);
            var viewModel = _cartModelFactory.CreateIndexViewModel(cart);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddItemToCart(AddItemToCartPostModel model)
        {
            if (ModelState.IsValid)
            {
                var cart = await _cartContext.GetOrCreateCartAsync(SessionsCartId);
                var productToRent = await _productContext.FindProductByIdOrDefaultAsync(model.ProductId);

                cart.AddItem(productToRent, model.RentPeriodInDays);

                await _cartContext.SaveChangesAsync();
            }

            return RedirectToLocal(model.ReturnUrl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmCart(string returnUrl)
        {
            var cart = await _cartContext.GetOrCreateCartAsync(SessionsCartId);

            cart.ConfirmCart();

            await _cartContext.SaveChangesAsync();

            _orderEventPublisher.PlaceOrder(cart);

            return RedirectToLocal(returnUrl);
        }
    }
}