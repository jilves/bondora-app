﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Retailer.Domain;
using Retailer.WebUi.Models;

namespace Retailer.WebUi.Controllers
{
    public class InvoiceController : Controller
    {
        private readonly InvoiceGenerator _invoiceGenerator;
        private readonly ICartContext _cartContext;

        public InvoiceController(InvoiceGenerator invoiceGenerator, ICartContext cartContext)
        {
            _invoiceGenerator = invoiceGenerator;
            _cartContext = cartContext;
        }

        public async Task<FileResult> DownloadInvoiceInPlainText(Guid cartId)
        {
            var cart = await _cartContext.GetOrCreateCartAsync(cartId);
            var reportBytes = await _invoiceGenerator.GeneratePlainTextInvoiceAsync(cart);

            return File(reportBytes, "text/plain", "Invoice.txt");
        }
    }
}