﻿using System;
using System.Web.Mvc;

namespace Retailer.WebUi.Controllers
{
    public class BaseController : Controller
    {
        protected Guid SessionsCartId => (Guid)(Session["CartId"] ?? (Session["CartId"] = Guid.NewGuid()));

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
