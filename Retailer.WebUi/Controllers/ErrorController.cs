﻿using System.Web.Mvc;

namespace Retailer.WebUi.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult Error404()
        {
            return View("Error404");
        }
    }
}
