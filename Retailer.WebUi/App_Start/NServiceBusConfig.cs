﻿using Contracts;
using Ninject;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Persistence.Legacy;
using Retailer.WebUi;
using WebActivatorEx;

[assembly: ApplicationShutdownMethod(typeof(NServiceBusConfig), "Stop")]
namespace Retailer.WebUi
{
    public class NServiceBusConfig
    {
        private static IStartableBus _bus;

        public static IBus CreateServiceBus(IKernel kernel)
        {
            BusConfiguration busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName(GlobalSettings.Default.RetailerBusEndpointName);
            busConfiguration.UseSerialization<JsonSerializer>();
            busConfiguration.EnableInstallers();

            busConfiguration.DisableFeature<TimeoutManager>();
            busConfiguration.UsePersistence<MsmqPersistence>();

            busConfiguration.Conventions().DefiningEventsAs(type => type.Namespace != null
                && type.Namespace.StartsWith("Contracts")
                && type.Namespace.EndsWith("Events"));

            busConfiguration.Conventions().DefiningCommandsAs(type => type.Namespace != null
                && type.Namespace.StartsWith("Contracts")
                && type.Namespace.EndsWith("Commands"));

            busConfiguration.UseContainer<NinjectBuilder>(p => p.ExistingKernel(kernel));

            return (_bus = Bus.Create(busConfiguration)).Start();
        }

        public static void Stop()
        {
            _bus.Dispose();
        }
    }
}