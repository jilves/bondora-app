﻿using System;
using Contracts;
using NUnit.Framework;
using Retailer.Domain.Entities;

namespace Retailer.Domain.UnitTests.Entities
{
    [TestFixture]
    public class CartItemTests
    {
        [TestCase(-1, ExpectedException = typeof(ArgumentException))]
        [TestCase(0, ExpectedException = typeof(ArgumentException))]
        [TestCase(1)]
        public void Create_Throws_When_RentPeriodInDays_Is_Not_A_Positive_Integer(int rentPeriodInDays)
        {
            CartItem.Create(Guid.NewGuid(), rentPeriodInDays);
        }

        // 2.2.2 Loyalty points
        // Customers get loyalty points when renting equipment.A heavy machine gives 2 points and other
        // types give one point per rental (regardless of the time rented).

        [TestCase(ProductType.SpecializedEquipment, 1)]
        [TestCase(ProductType.RegularEquipment, 1)]
        [TestCase(ProductType.HeavyEquipment, 2)]
        public void CalculateLoyalityPoints(ProductType productType, int expectedLoyalityPoints)
        {
            var cartItem = CartItem.Create(Guid.NewGuid(), rentPeriodInDays: 100);
            cartItem.Product = Product.Create(Guid.NewGuid(), "product", true, productType);

            Assert.That(cartItem.CalculateLoyalityPoints(), Is.EqualTo(expectedLoyalityPoints));
        }

        //  2.2.1 Price calculation
        //  The price of rentals is based on equipment type and rental length.
        //  There are three different fees:

        //   One-time rental fee – 100€
        //   Premium daily fee – 60€/day
        //   Regular daily fee – 40€/day

        //  The price calculation for different types of equipment is:

        //   Heavy – rental price is one-time rental fee plus premium fee for each day rented.

        //   Regular – rental price is one-time rental fee plus premium fee for the first 2 days plus regular
        //  fee for the number of days over 2.

        //   Specialized – rental price is premium fee for the first 3 days plus regular fee times the
        //  number of days over 3.

        [TestCase(ProductType.HeavyEquipment, 1, 100 + 60)]
        [TestCase(ProductType.HeavyEquipment, 2, 100 + 60 + 60)]

        [TestCase(ProductType.RegularEquipment, 1, 100 + 60)]
        [TestCase(ProductType.RegularEquipment, 2, 100 + 60 + 60)]
        [TestCase(ProductType.RegularEquipment, 3, 100 + 60 + 60 + 40)]

        [TestCase(ProductType.SpecializedEquipment, 1, 60)]
        [TestCase(ProductType.SpecializedEquipment, 2, 60 + 60)]
        [TestCase(ProductType.SpecializedEquipment, 3, 60 + 60 + 60)]
        [TestCase(ProductType.SpecializedEquipment, 4, 60 + 60 + 60 + 40)]

        public void CalculateCost(ProductType productType, int daysRented, decimal expectedCost)
        {
            var cartItem = CartItem.Create(Guid.NewGuid(), daysRented);
            cartItem.Product = Product.Create(Guid.NewGuid(), "product", true, productType);

            Assert.That(cartItem.CalculateCost(), Is.EqualTo(expectedCost));
        }
    }
}
