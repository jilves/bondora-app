﻿using System;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain
{
    public interface IOrderContext : IDisposable
    {
        Order AddOrder(Order order);
        int SaveChanges();
    }
}