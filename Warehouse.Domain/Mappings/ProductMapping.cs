﻿using Warehouse.Domain.Entities;

namespace Warehouse.Domain.Mappings
{
    public class ProductMapping : BaseEntityMapping<Product>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(product => product.Id);

            Entity
                .HasMany(product => product.OrderItems)
                .WithRequired(orderItem => orderItem.Product)
                .HasForeignKey(orderItem => orderItem.ProductId);

            Entity
                .Property(product => product.Name)
                .IsRequired()
                .IsUnicode();

        }
    }
}
