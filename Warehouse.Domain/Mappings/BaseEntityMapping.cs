﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace Warehouse.Domain.Mappings
{
    public abstract class BaseEntityMapping<TEntityType> where TEntityType : class
    {
        private Func<EntityTypeConfiguration<TEntityType>> _entityConfig;

        protected EntityTypeConfiguration<TEntityType> Entity => _entityConfig();

        public void Map(DbModelBuilder modelBuilder)
        {
            _entityConfig = modelBuilder.Entity<TEntityType>;

            MapEntity();
        }

        protected abstract void MapEntity();
    }
}
