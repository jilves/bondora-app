﻿using Warehouse.Domain.Entities;

namespace Warehouse.Domain.Mappings
{
    public class OrderMapping : BaseEntityMapping<Order>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(order => order.Id);

            Entity
                .HasMany(order => order.OrderItems)
                .WithRequired(orderItem => orderItem.Order)
                .HasForeignKey(orderItem => orderItem.OrderId);
        }
    }
}
