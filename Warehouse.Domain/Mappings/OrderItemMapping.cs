﻿using Warehouse.Domain.Entities;

namespace Warehouse.Domain.Mappings
{
    public class OrderItemMapping : BaseEntityMapping<OrderItem>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(orderItem => orderItem.Id);

            Entity
                .HasRequired(orderItem => orderItem.Order)
                .WithMany(order => order.OrderItems)
                .HasForeignKey(orderItem => orderItem.OrderId)
                .WillCascadeOnDelete(false);

            Entity
                .HasRequired(orderItem => orderItem.Product)
                .WithMany(product => product.OrderItems)
                .HasForeignKey(orderItem => orderItem.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}
