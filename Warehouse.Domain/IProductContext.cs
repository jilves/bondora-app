﻿using System;
using System.Collections.Generic;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain
{
    public interface IProductContext : IDisposable
    {
        Product AddProduct(Product product);
        IReadOnlyCollection<Product> AllProducts();
        int SaveChanges();
    }
}