﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Domain
{
    public class DbContextFactory
    {
        private readonly string _connectionstringName;

        public DbContextFactory(string connectionstringName)
        {
            _connectionstringName = connectionstringName;
        }

        public IOrderContext CreateOrderContext()
        {
            return new DomainContext(_connectionstringName);
        }


        public IProductContext CreateProductContext()
        {
            return new DomainContext(_connectionstringName);
        }
    }
}
