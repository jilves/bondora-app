﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Domain.Entities;
using Warehouse.Domain.Mappings;

namespace Warehouse.Domain
{
    public class DomainContext : DbContext, IOrderContext, IProductContext
    {
        static DomainContext()
        {
            Database.SetInitializer(new DomainInitializer());
        }

        public DomainContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            // For entity framework connection string's '|AttachDbFilename=|DataDirectory|\Warehouse.mdf'

            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        private DbSet<Product> Products => Set<Product>();
        private DbSet<Order> Orders => Set<Order>();

        // http://stackoverflow.com/a/15820506/5499167
        public async override Task<int> SaveChangesAsync()
        {
            try
            {
                return await base.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            new OrderMapping().Map(modelBuilder);
            new OrderItemMapping().Map(modelBuilder);
            new ProductMapping().Map(modelBuilder);
        }

        // IProductContext impl

        public Product AddProduct(Product product)
        {
            return Products.Add(product);
        }

        public IReadOnlyCollection<Product> AllProducts()
        {
            return Products
                .ToList()
                .AsReadOnly();
        }

        // IOrderContext impl

        public Order AddOrder(Order order)
        {
            return Orders.Add(order);
        }
    }
}
