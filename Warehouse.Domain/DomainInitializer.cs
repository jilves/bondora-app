﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain
{
    public class DomainInitializer : DropCreateDatabaseAlways<DomainContext>
    {
        protected override void Seed(DomainContext context)
        {
            var productTemplates = new List<Product>
            {
                Product.Create("Caterpillar bulldozer", ProductType.HeavyEquipment),
                Product.Create("KamAZ truck", ProductType.RegularEquipment),
                Product.Create("Komatsu crane", ProductType.HeavyEquipment),
                Product.Create("Volvo steamroller", ProductType.RegularEquipment),
                Product.Create("Bosch jackhammer", ProductType.SpecializedEquipment)
            };

            productTemplates.ForEach(product =>
            {
                Enumerable.Range(0, 3).ToList().ForEach(i =>
                {
                    var newProduct = Product.Create(product.Name + " " + i, product.ProductType);

                    context.AddProduct(newProduct);
                });
            });

            context.SaveChanges();
        }
    }
}