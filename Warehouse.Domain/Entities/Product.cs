﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contracts;

namespace Warehouse.Domain.Entities
{
    public class Product : IAggregateRoot
    {
        private ICollection<OrderItem> _orderItems;

        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public ProductType ProductType { get; protected set; }

        protected internal virtual ICollection<OrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<OrderItem>()); }
            set { _orderItems = value; }
        }

        protected Product() { }

        public static Product Create(string name, ProductType productType)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException(string.Empty, nameof(name));
            }

            return new Product
            {
                Id = Guid.NewGuid(),
                Name = name,
                ProductType = productType
            };
        }

        // For demo purposes, if any orders exist for the product it's considered as rented out.
        // There is no quantity field implemented.

        public bool IsAvailableForRent()
        {
            return !OrderItems.Any();
        }
    }
}
