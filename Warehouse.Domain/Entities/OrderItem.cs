﻿using System;

namespace Warehouse.Domain.Entities
{
    public class OrderItem
    {
        public Guid Id { get; protected set; }
        public Guid OrderId { get; protected set; }
        public Guid ProductId { get; protected set; }

        protected internal virtual Product Product { get; set; }
        protected internal virtual Order Order { get; set; }

        protected OrderItem() { }

        public static OrderItem Create(Guid productId)
        {
            return new OrderItem
            {
                Id = Guid.NewGuid(),
                ProductId = productId
            };
        }
    }
}
