﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Warehouse.Domain.Entities
{
    public class Order : IAggregateRoot
    {
        private ICollection<OrderItem> _orderItems;

        public Guid Id { get; protected set; }

        protected internal virtual ICollection<OrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<OrderItem>()); }
            set { _orderItems = value; }
        }

        protected Order() { }

        public static Order Create(Guid id, IEnumerable<OrderItem> items)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("Empty Guid", nameof(id));
            }

            var orderItems = items as OrderItem[] ?? items.ToArray();
            if (!orderItems.Any())
            {
                throw new ArgumentException("Cannot create an order without orderitems.", nameof(items));
            }
            
            return new Order
            {
                Id = id,
                OrderItems = orderItems
            };
        }
    }
}
