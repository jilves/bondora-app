﻿using Contracts;
using NUnit.Framework;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.UnitTests.Entities
{
    [TestFixture]
    public class ProductTests
    {
        [Test]
        public void IsAvailableForRent_Returns_True_If_No_Pending_Loans()
        {
            var product = Product.Create("p", ProductType.HeavyEquipment);
            
            Assert.That(product.IsAvailableForRent(), Is.True);
        }

        [Test]
        public void IsAvailableForRent_Returns_False_If_Any_Pending_Loans()
        {
            var product = Product.Create("p", ProductType.HeavyEquipment);

            product.OrderItems.Add(null);

            Assert.That(product.IsAvailableForRent(), Is.False);
        }
    }
}
