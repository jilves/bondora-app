﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using NUnit.Framework;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.UnitTests.Entities
{
    [TestFixture]
    public class OrderTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_Throws_On_Empty_Guid()
        {
            Order.Create(Guid.Empty, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_Throws_On_Empty_OrderItems()
        {
            Order.Create(Guid.NewGuid(), Enumerable.Empty<OrderItem>());
        }

        [Test]
        public void Create_Creates_New_Order()
        {
            var orderId = Guid.NewGuid();
            var orderItems = new List<OrderItem>
            {
               OrderItem.Create(Guid.NewGuid())
            };

            // Act
            var newOrder = Order.Create(orderId, orderItems);

            // Assert
            Assert.That(newOrder.Id, Is.EqualTo(orderId));
            Assert.That(newOrder.OrderItems, Is.EquivalentTo(orderItems));
        }
    }
}
