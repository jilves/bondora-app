﻿using Retailer.Domain.Entities;

namespace Retailer.Domain.Mappings
{
    public class ProductMapping : BaseEntityMapping<Product>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(product => product.Id);

            Entity
                .HasMany(product => product.CartItems)
                .WithRequired(cartItem => cartItem.Product)
                .HasForeignKey(cartItem => cartItem.ProductId);

            Entity
                .Property(product => product.Name)
                .IsRequired()
                .IsUnicode();
        }
    }
}
