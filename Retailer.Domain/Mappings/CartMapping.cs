﻿using Retailer.Domain.Entities;

namespace Retailer.Domain.Mappings
{
    public class CartMapping : BaseEntityMapping<Cart>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(cart => cart.Id);

            Entity
                .HasMany(cart => cart.CartItemsCollection)
                .WithRequired(cartItem => cartItem.Cart)
                .HasForeignKey(cartItem => cartItem.CartId);
        }
    }
}
