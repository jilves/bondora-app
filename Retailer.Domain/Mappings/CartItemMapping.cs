﻿using Retailer.Domain.Entities;

namespace Retailer.Domain.Mappings
{
    public class CartItemMapping : BaseEntityMapping<CartItem>
    {
        protected override void MapEntity()
        {
            Entity.HasKey(cartItem => cartItem.Id);

            Entity
                .HasRequired(cartItem => cartItem.Cart)
                .WithMany(cart => cart.CartItemsCollection)
                .HasForeignKey(cartItem => cartItem.CartId)
                .WillCascadeOnDelete(false);

            Entity
                .HasRequired(cartItem => cartItem.Product)
                .WithMany(product => product.CartItems)
                .HasForeignKey(cartItem => cartItem.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}
