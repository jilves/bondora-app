﻿using System;
using System.Collections.Generic;
using Contracts;

namespace Retailer.Domain.Entities
{
    public class Product : IAggregateRoot
    {
        private ICollection<CartItem> _cartItems;
        protected Product() { }

        public Guid Id { get; protected set; }

        public string Name { get; protected internal set; }
        public ProductType ProductType { get; protected internal set; }

        protected internal virtual ICollection<CartItem> CartItems
        {
            get { return _cartItems ?? (_cartItems = new List<CartItem>()); }
            set { _cartItems = value; }
        }

        /// <summary>
        /// In production, this would be Quantity or a reference to the stock table. 
        /// The value of this property is determined by the warehouse.
        /// </summary>
        public bool IsAvailableForRent { get; protected internal set; }

        public void UpdateProduct(ProductDto dto)
        {
            Name = dto.Name;
            ProductType = dto.ProductType;
            IsAvailableForRent = dto.IsAvailableForRent;
        }

        public static Product Create(Guid id, string name, bool isAvailableForRent, ProductType productType)
        {
            return new Product
            {
                Id = id,
                Name = name,
                IsAvailableForRent = isAvailableForRent,
                ProductType = productType
            };
        }
    }
}
