﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;

namespace Retailer.Domain.Entities
{
    public class Cart : IAggregateRoot
    {
        private ICollection<CartItem> _cartItemsCollection;

        public Guid Id { get; protected set; }

        public bool IsConfirmed { get; protected set; }

        public OrderStatusCode? OrderStatusCode { get; protected set; }

        protected internal virtual ICollection<CartItem> CartItemsCollection
        {
            get { return _cartItemsCollection ?? (_cartItemsCollection = new List<CartItem>()); }
            set { _cartItemsCollection = value; }
        }

        public IReadOnlyCollection<CartItem> CartItems => CartItemsCollection
            .ToList()
            .AsReadOnly(); 

        protected Cart() { }

        public static Cart Create(Guid id)
        {
            return new Cart
            {
                Id = id
            };
        }

        public void AddItem(Product product, int rentPeriodInDays)
        {
            if (!product.IsAvailableForRent)
            {
                throw new Exception($"Product {product.Name} with Id {product.Id} is out of stock!");
            }

            if (1 > rentPeriodInDays)
            {
                throw new ArgumentException($"{nameof(rentPeriodInDays)} must be equal to or bigger than 1!", nameof(rentPeriodInDays));
            }

            CartItemsCollection.Add(CartItem.Create(product.Id, rentPeriodInDays));
        }

        public void ConfirmCart()
        {
            IsConfirmed = true;
        }

        public void UpdateStatus(OrderStatusCode statusCode)
        {
            OrderStatusCode = statusCode;
        }

        public decimal CalculateTotalCost()
        {
            return CartItemsCollection.Sum(cartItem => cartItem.CalculateCost());
        }

        public int CalculateTotalLoyalityPoints()
        {
            return CartItemsCollection.Sum(cartItem => cartItem.CalculateLoyalityPoints());
        }
    }
}
