﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Contracts;

namespace Retailer.Domain.Entities
{
    public class CartItem
    {
        public Guid Id { get; protected set; }
        public Guid CartId { get; protected set; }
        public Guid ProductId { get; protected set; }

        public int RentPeriodInDays { get; protected set; }

        protected internal virtual Product Product { get; set; }
        protected internal virtual Cart Cart { get; set; }

        [NotMapped]
        public string ProductName => Product.Name;

        [NotMapped]
        public ProductType ProductType => Product.ProductType;

        protected CartItem() { }

        internal static CartItem Create(Guid productId, int rentPeriodInDays)
        {
            if (1 > rentPeriodInDays)
            {
                throw new ArgumentException($"{nameof(rentPeriodInDays)} must be equal to or larger than 1!", nameof(rentPeriodInDays));
            }

            return new CartItem
            {
                Id = Guid.NewGuid(),
                ProductId = productId,
                RentPeriodInDays = rentPeriodInDays
            };
        }

        public virtual int CalculateLoyalityPoints()
        {
            switch (Product.ProductType)
            {
                case ProductType.HeavyEquipment:
                    return 2;
                case ProductType.RegularEquipment:
                    return 1;
                case ProductType.SpecializedEquipment:
                    return 1;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private const decimal OneTimeRentalFee = 100;
        private const decimal PremiumDailyFee = 60;
        private const decimal RegularDailyFee = 40;

        public virtual decimal CalculateCost()
        {
            switch (Product.ProductType)
            {
                case ProductType.HeavyEquipment:
                    return CalculateHeavyEquipmentCost();
                case ProductType.RegularEquipment:
                    return CalculateRegularEquipmentCost();
                case ProductType.SpecializedEquipment:
                    return CalculateSpecializedEquipmentCost();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private decimal CalculateHeavyEquipmentCost()
        {
            return OneTimeRentalFee + (RentPeriodInDays * PremiumDailyFee);
        }

        private decimal CalculateRegularEquipmentCost()
        {
            return OneTimeRentalFee + Enumerable.Range(1, RentPeriodInDays)
                .Select(rentDay => rentDay > 2 
                        ? RegularDailyFee
                        : PremiumDailyFee)
                        .Sum();
        }

        private decimal CalculateSpecializedEquipmentCost()
        {
            return Enumerable
                .Range(1, RentPeriodInDays)
                .Select(rentDay => rentDay > 3
                    ? RegularDailyFee
                    : PremiumDailyFee)
                    .Sum();
        }
    }
}
