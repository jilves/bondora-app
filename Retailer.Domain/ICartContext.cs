﻿using System;
using System.Threading.Tasks;
using Retailer.Domain.Entities;

namespace Retailer.Domain
{
    public interface ICartContext : IDisposable
    {
        Task<Cart> GetOrCreateCartAsync(Guid cartId);
        Task<int> SaveChangesAsync();
    }
}