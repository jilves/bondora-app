﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Retailer.Domain.Entities;

namespace Retailer.Domain
{
    public interface IProductContext : IDisposable
    {
        Task<IReadOnlyCollection<Product>> GetProductsAvailableForRentAsync();
        Task<Product> FindProductByIdOrDefaultAsync(Guid productId);

        int SaveChanges();
        void AddProduct(Product newProduct);
    }
}