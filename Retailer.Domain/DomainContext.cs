﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Retailer.Domain.Entities;
using Retailer.Domain.Mappings;

namespace Retailer.Domain
{
    public class DomainContext : DbContext, ICartContext, IProductContext
    {
        private DbSet<Cart> Carts => Set<Cart>();
        private DbSet<Product> Products => Set<Product>();

        static DomainContext()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<DomainContext>());
        }


        public DomainContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            
        }

        // http://stackoverflow.com/a/15820506/5499167
        public async override Task<int> SaveChangesAsync()
        {
            try
            {
                return await base.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public void AddProduct(Product newProduct)
        {
            Products.Add(newProduct);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            new CartMapping().Map(modelBuilder);
            new CartItemMapping().Map(modelBuilder);
            new ProductMapping().Map(modelBuilder);
        }

        // ICartContext impl

        public async Task<Cart> GetOrCreateCartAsync(Guid cartId)
        {
            var cart = await Carts.SingleOrDefaultAsync(c => c.Id == cartId);

            return cart ?? Carts.Add(Cart.Create(cartId));
        }

        // IProductContext impl

        public async Task<IReadOnlyCollection<Product>> GetProductsAvailableForRentAsync()
        {
            var products = await Products.Where(product => product.IsAvailableForRent)
                .ToListAsync();

            return products.AsReadOnly();
        }

        public async Task<Product> FindProductByIdOrDefaultAsync(Guid productId)
        {
            return await Products.FirstOrDefaultAsync(product => product.Id == productId);
        }
    }
}
