### How do I get set up? ###

Assuming you have MSMQ (Microsoft Message Queuing) enabled on your operating system, then the solution should be  CTRL+F5 ready in Visual Studio 2015.


### How does it work? ###

The solution has 2 tiers, a "warehouse" app and a "retailer" app.

Both depend on NServiceBus for message based communication and Entity Framework for data persistence.


### The database structure ###

See the corresponding .Domain projects or database_structure.png to get an overview of the database structure.


### The warehouse ###

Every time you run the Warehouse.Service console app, it will:

* Create a new local database with rentable products
* Publish those products as "ProductWasAddedEvent"

You can insert new products with random product type in the warehouse console app by typing in the name of the 
product and pressing enter.

The warehouse accepts "PlaceOrder" commands and responds with "OrderWasProcessed" events. 

If an order was succesfully processed (OrderWasProcessedEvent.cs -> StatusCode == Success),  a "ProductWasUpdated" event
is also published for every rented product for syncing retailers.

Concurrent orders are solved with a simple object lock. If a product is out of stock (has 1 existing order), an OrderWasProcessed event
with StatusCode == OutOfStock is returned.

### The retailer ###

Every time you run the Retailer.WebUi web app, it will:

* Create a new empty local database
* Add a product to the database for every "ProductWasAddedEvent" event

In theory you can copy paste the retailer app code and have many retailer nodes all communicating with the warehouse,
but this is untested.

The retailer app sends "PlaceOrder" commands and accepts "OrderWasProccessed" and "ProductWasUpdated" events.

The UI flow ends with confirming the cart and printing out the invoice to save development time.
If you want to create a new cart you have to manually delete the cookie containing the cart session info.


### Logging ###

Communication is logged using NLog and saved as log.txt for both applications.
ASP.NET MVC also has Elmah enabled with custom errors turned on for 404 and general errors.

### Messaging contracts ###

Both applications depend on the Contracts class library for coding convenience.
The Contracts library defines all possible events and commands as POCO objects.


